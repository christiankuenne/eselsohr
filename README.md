# Eselsohr

Simple bookmark manager

## Installation

```
sudo make install
```

## Usage

```
eselsohr FILE COMMAND ARGS
```

Add new bookmark: `eselsohr FILE add URL NAME CATEGORY`

Delete bookmark: `eselsohr FILE delete URL`

Show rofi interface: `eselsohr FILE rofi TITLE`

Examples:

```
eselsohr bookmarks.txt add https://www.spiegel.de "Der Spiegel" News
eselsohr bookmarks.txt delete https://www.bild.de
eselsohr bookmarks.txt rofi Bookmarks
```

## Rofi interface

It is recommended to setup a keybord shortcut to show the rofi interface. Check out how to setup keyboard shortcuts in your desktop environment or window manager. 

`Alt+n`: Add new bookmark. Only works if the browser window is active. The URL is copied automatically from the address bar of the browser.

`Alt+d`: Delete selected bookmark.

## Import/Export

There is no import yet. The following exports are available:

* json
* xml
* markdown
* html (can be used to import bookmarks to Firefox and Chrome)

```
eselsohr-export SOURCE TITLE TARGET
```

Examples:

```
eselsohr-export bookmarks.txt Bookmarks bookmarks.json
eselsohr-export bookmarks.txt Bookmarks bookmarks.xml
eselsohr-export bookmarks.txt Bookmarks bookmarks.md
eselsohr-export bookmarks.txt Bookmarks bookmarks.html
```

## TODO

* Edit Bookmark
* Import from Firefox / Chrome
