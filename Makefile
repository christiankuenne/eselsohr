install:
	cp eselsohr /usr/bin/
	cp eselsohr-export /usr/bin/
	chmod +x /usr/bin/eselsohr
	chmod +x /usr/bin/eselsohr-export

clean:
	rm /usr/bin/eselsohr
	rm /usr/bin/eselsohr-export
